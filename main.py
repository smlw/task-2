import numpy as np
import time
items = []  # Список куда записываются сгенерированные числа

sourceLineTimes = 10000 # Сколько исходных строк
learnLineTimes = 5000 # Сколько строк обучить

# Генерация шести чесел
def genOut():
    for i in range(sourceLineTimes):
        item = np.array(np.random.uniform(-1, 1, size=5))  # Генерим первые 5 чисел
        bin = np.random.randint(2, size=1)  # генерим шестое число
        item = np.append(item, bin)  # Добавляем шестое число
        items.append(item)  # Сохраняем список из шести чисел

    writeState(items, 'csv/sourceFile.csv')  # Пишем в файл
    items.clear() # Чистим список
    print('Сгенерированы классифицированные данные')

# Генерация пяти чисел
def genIn():
    for i in range(learnLineTimes):
        item = np.array(np.random.uniform(-1, 1, size=6))  # Генерим первые 5 чисел
        items.append(item)  # Сохраняем список из шести чисел

    writeState(items, 'csv/learnFile.csv')  # Пишем в файл
    items.clear()  # Чистим список
    print('Сгенерированы не классифицированные данные')


# Функция записи в файл
def writeState(data, path):
    np.savetxt(path, data, delimiter=";", fmt='%.7f')



start_time = time.time()

genOut()
genIn()

print("--- %s End seconds ---" % (time.time() - start_time))

