import numpy as np
import time
from math import sqrt

# Ищем занчения
def findSqrl():

    print('Классификация началась ...')

    source = np.genfromtxt('csv/sourceFile.csv', delimiter=';')
    learn = np.genfromtxt('csv/learnFile.csv', delimiter=';')

    for l in learn: # Итерируем learn
        minway = 100.0 # значение для выявление минимального
        classObj = None # Будет храниться класс минималного расстояния
        for s in source: #Итерируем Souurse

            # Формула:
            current = ((s[0] - l[0]) ** 2) + ((s[1] - l[1]) ** 2) + ((s[2] - l[2]) ** 2) + ((s[3] - l[3]) ** 2) + ((s[4] - l[4]) ** 2)
            current = sqrt(current)

            # Проверяем меньше ли расстояние предыдущего
            if(current < minway):
                minway = current
                classObj = s[5]

        l[5] = classObj # Пишем класс не классифицированному файлу
    np.savetxt('csv/learnFile.csv', learn, delimiter=";", fmt='%.7f')

    print('Классификация окончена')


start_time = time.time()

findSqrl()

print("--- %s End seconds ---" % (time.time() - start_time))